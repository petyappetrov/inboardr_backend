import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { UsersModule } from './users/users.module'
import { AuthModule } from './auth/auth.module'
import { SnowboardsModule } from './snowboards/snowboards.module'
import { UploadModule } from './upload/upload.module'
import { OrdersModule } from './orders/orders.module'
import { OffersModule } from './offers/offers.module'
import { FirebaseModule } from './firebase/firebase.module';
import { NotificationsModule } from './notifications/notifications.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const host = configService.get('DB_HOST')
        const port = configService.get('DB_PORT')
        const name = configService.get('DB_NAME')
        const username = configService.get('DB_USERNAME')
        const password = configService.get('DB_PASSWORD')
        return {
          uri: `mongodb://${username}:${password}@${host}:${port}/${name}`,
          useCreateIndex: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          useNewUrlParser: true,
        }
      },
    }),
    UsersModule,
    AuthModule,
    SnowboardsModule,
    UploadModule,
    OrdersModule,
    OffersModule,
    FirebaseModule,
    NotificationsModule,
  ],
})
export class AppModule {}
