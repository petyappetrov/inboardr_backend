import { Document, Types } from 'mongoose'

export interface FCMMessage extends Document {
  token: string
  title: string
  body?: string
  data?: any
}
