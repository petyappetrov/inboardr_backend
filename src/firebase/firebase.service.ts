import { Injectable, OnModuleInit } from '@nestjs/common'
import * as admin from 'firebase-admin'

@Injectable()
export class FirebaseService implements OnModuleInit {
  admin: admin.app.App

  onModuleInit() {
    this.admin = admin.initializeApp({
      credential: admin.credential.applicationDefault(),
      databaseURL: 'https://boardr-b0624.firebaseio.com',
    })
  }

  send({
    token,
    title,
    body,
    data,
  }: {
    token: string
    title?: string
    body?: string
    data?: any
  }) {
    return this.admin.messaging().send({
      token: token,
      notification: {
        title: title,
        body: body,
      },
      data: data,
    })
  }

  sendMulticast({
    tokens,
    title,
    body,
    data,
  }: {
    tokens: string[]
    title?: string
    body?: string
    data?: any
  }) {
    return this.admin.messaging().sendMulticast({
      tokens: tokens,
      notification: {
        title: title,
        body: body,
      },
      data: data,
    })
  }
}
