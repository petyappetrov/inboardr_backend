import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { UploadController } from './upload.controller'

@Module({
  providers: [ConfigModule],
  controllers: [UploadController],
})
export class UploadModule {}
