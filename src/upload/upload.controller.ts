import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFiles,
  UseGuards,
} from '@nestjs/common'
import { FilesInterceptor } from '@nestjs/platform-express'
import { AuthGuard } from '@nestjs/passport'
import * as multer from 'multer'
import * as path from 'path'
import * as uuid from 'uuid/v1'
import { ConfigService } from '@nestjs/config'

@Controller('upload')
export class UploadController {
  constructor(private readonly configService: ConfigService) {}
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FilesInterceptor('files', 10, {
      storage: multer.diskStorage({
        destination: './public/upload/',
        filename: (_, file, callback) => {
          const extension = path.extname(file.originalname)
          const name = file.originalname.split('.')[0].replace(/ +/g, '-')
          const uniqueId = uuid()
          callback(null, `${name}-${uniqueId}${extension}`)
        },
      }),
    }),
  )
  upload(@UploadedFiles() files: [{ filename: string; path: string }]) {
    const urls = files.map((file) => {
      const path = file.path.replace('public', '')
      const port = this.configService.get('API_PORT')
      const host = this.configService.get('API_HOST')
      return `http://${host}:${port}${path}`
    })
    return urls
  }
}
