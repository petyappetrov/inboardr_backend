import {
  IsNotEmpty,
  MinLength,
  IsString,
  IsNumber,
  IsOptional,
  IsDateString,
} from 'class-validator'

export class CreateOrderInput {
  @IsNotEmpty()
  @IsDateString()
  readonly plannedDate: Date

  @IsNotEmpty()
  @IsNumber()
  readonly sizeBoots: number

  @IsNotEmpty()
  @IsNumber()
  readonly sizeBoard: number

  @IsNotEmpty()
  @IsNumber()
  readonly price: number

  @IsOptional()
  @IsString()
  @MinLength(4)
  readonly comment?: string
}
