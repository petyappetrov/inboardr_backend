import {
  MinLength,
  IsString,
  IsEnum,
  IsDateString,
  IsNumber,
} from 'class-validator'

export class CreateOrderInput {
  @IsDateString()
  readonly plannedDate?: Date

  @IsNumber()
  readonly sizeBoots?: number

  @IsNumber()
  readonly sizeBoard?: number

  @IsNumber()
  readonly price?: number

  @MinLength(4)
  @IsString()
  readonly comment?: string

  @IsEnum(['pending', 'accepted', 'closed', 'rejected', 'cancelled'])
  readonly status?: 'pending' | 'accepted' | 'closed' | 'rejected' | 'cancelled'
}
