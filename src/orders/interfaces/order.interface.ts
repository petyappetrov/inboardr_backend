import { Document, Types } from 'mongoose'

export interface Order extends Document {
  readonly creator: Types.ObjectId
  readonly status: 'pending' | 'accepted' | 'rejected' | 'cancelled'
  readonly plannedDate: Date
  readonly sizeBoots: number
  readonly sizeBoard: number
  readonly price: number
  readonly comment?: string
}
