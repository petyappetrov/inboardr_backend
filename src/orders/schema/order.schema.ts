import * as mongoose from 'mongoose'

export const OrderSchema = new mongoose.Schema(
  {
    creator: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    acceptedOffer: {
      type: mongoose.Types.ObjectId,
      ref: 'Offer',
    },
    status: {
      type: String,
      enum: ['pending', 'accepted', 'closed', 'rejected', 'cancelled'],
      default: 'pending',
    },
    plannedDate: {
      type: Date,
      required: true,
    },
    sizeBoots: {
      type: Number,
      required: true,
    },
    sizeBoard: {
      type: Number,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    comment: String,
  },
  {
    timestamps: true,
  },
)
