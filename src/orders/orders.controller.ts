import {
  Controller,
  Post,
  Body,
  Param,
  Delete,
  Get,
  UseGuards,
  Request,
  NotFoundException,
  NotAcceptableException,
  Put,
} from '@nestjs/common'
import { Request as RequestType } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { OrdersService } from './orders.service'
import { CreateOrderInput } from './inputs/create-order.input'
import { User } from '../users/interfaces/user.interface'

@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  createOrder(
    @Body() input: CreateOrderInput,
    @Request() request: RequestType & { user: User },
  ) {
    return this.ordersService.createOrder({ ...input, creator: request.user._id })
  }

  @Put('/cancel/:id')
  @UseGuards(AuthGuard('jwt'))
  cancelOrder(
    @Param('id') id: string,
  ) {
    /**
     * Добавить валидацию
     * Отменять заказ может только владелец заказа
     */
    return this.ordersService.cancelOrder(id)
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async deleteOrder(
    @Param('id') id: string,
    @Request() request: RequestType & { user: User },
  ) {
    const order = await this.ordersService.getOrderById(id)
    if (!order) {
      throw new NotFoundException()
    }
    if (!order.creator.equals(request.user._id)) {
      throw new NotAcceptableException()
    }
    return this.ordersService.deleteOrder(id)
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  getOrder(@Param('id') id: string) {
    return this.ordersService.getOrderById(id)
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  getOrders() {
    return this.ordersService.getOrders()
  }
}
