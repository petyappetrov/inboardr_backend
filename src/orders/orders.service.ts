import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model, Types } from 'mongoose'
import { Order } from './interfaces/order.interface'
import { CreateOrderInput } from './inputs/create-order.input'
import { SnowboardsService } from '../snowboards/snowboards.service'
import { Snowboard } from '../snowboards/interfaces/snowboard.interface'
import { NotificationsService } from '../notifications/notifications.service'
import { NotificationTypes } from 'src/notifications/interfaces/notifications.interface'

@Injectable()
export class OrdersService {
  constructor(
    @InjectModel('Order') private readonly orderModel: Model<Order>,
    private readonly snowboardsService: SnowboardsService,
    private readonly notificationsService: NotificationsService,
  ) {}

  async createOrder(input: CreateOrderInput & { creator: string }): Promise<Order> {
    /**
     * Get all snowboards by size boots
     */
    const snowboards: Snowboard[] = await this.snowboardsService.getSnowboards({
      sizeBoots: input.sizeBoots,
      owner: {
        $ne: input.creator,
      }
    });

    /**
     * Filtering of snowboards to only have FCM token
     */
    const filteredSnowboards = snowboards.filter((s) => s.owner.tokenFCM)

    /**
     * If snowboards is not found send exception to client
     */
    if (!filteredSnowboards.length) {
      throw new NotFoundException('Not found');
    }

    /**
     * Create order
     */
    const order: Order = await this.orderModel.create(input)

    /**
     * Grouping snowboards by owner for sending of push-notifications
     */
    type SnowboardsGroupedByOwners = { [key: string]: Snowboard[] }
    const snowboardGroupedByOwners: SnowboardsGroupedByOwners = filteredSnowboards.reduce(
      (groupList: SnowboardsGroupedByOwners, snowboard: Snowboard) => {
        const hasGroup: Boolean = Object
          .keys(groupList)
          .some((id) => Types.ObjectId(snowboard.owner._id).equals(id))

        if (hasGroup) {
          return {
            ...groupList,
            [snowboard.owner._id]: [
              ...groupList[snowboard.owner._id],
              snowboard,
            ]
          }
        }

        return {
          ...groupList,
          [snowboard.owner._id]: [snowboard]
        }
      },
      {}
    )


    /**
     * Send push-notifications
     */
    await Promise.all(Object.keys(snowboardGroupedByOwners)
      .map(async (ownerId: string) => {
        const snowboards: Snowboard[] = snowboardGroupedByOwners[ownerId]
        await this.notificationsService.createNotification({
          receiver: ownerId,
          title: 'Boardr - Заказ',
          body: 'У вас новый заказ! Посмотрите его',
          type: NotificationTypes.OrderCreated,
          data: {
            orderId: JSON.stringify(order._id),
            snowboards: JSON.stringify(snowboards.map((snowboard) => snowboard._id))
          }
        })
      })
    )

    return order
  }


  cancelOrder(id: string): Promise<Order> {
    return this.orderModel.findByIdAndUpdate(id, { status: 'canceled' }).exec()
  }

  getOrders(): Promise<Order[]> {
    return this.orderModel.find({}).exec();
  }

  getOrderById(id: string | Types.ObjectId): Promise<Order> {
    return this.orderModel.findById(id).exec()
  }

  deleteOrder(id: string): Promise<Order> {
    return this.orderModel.findByIdAndDelete(id).exec()
  }
}
