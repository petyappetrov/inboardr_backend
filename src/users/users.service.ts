import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { User } from './interfaces/user.interface'
import { Model, Types } from 'mongoose'
import { UpdateUserInput } from './inputs/update-user.input'
import { CreateUserInput } from './inputs/create-user.input'

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  getUsers(): Promise<User[]> {
    return this.userModel.find({}).exec()
  }

  getUserById(id: string | Types.ObjectId): Promise<User> {
    return this.userModel.findById(id).exec()
  }

  getUserByPhone(phone: string): Promise<User> {
    return this.userModel.findOne({ phone }).exec()
  }

  createUser(input: CreateUserInput): Promise<User> {
    return this.userModel.create(input)
  }

  updateUserById(id: string, input: UpdateUserInput): Promise<User> {
    return this.userModel.findByIdAndUpdate(id, input, { new: true }).exec()
  }

  deleteUser(id: string): Promise<User> {
    return this.userModel.findByIdAndDelete(id).exec()
  }
}
