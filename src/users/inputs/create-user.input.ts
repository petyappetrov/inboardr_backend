import { IsNotEmpty, MinLength } from 'class-validator'

export class CreateUserInput {
  @IsNotEmpty()
  firstName: string

  @IsNotEmpty()
  lastName: string

  @IsNotEmpty()
  @MinLength(6)
  phone: string

  @IsNotEmpty()
  @MinLength(6)
  password: string
}
