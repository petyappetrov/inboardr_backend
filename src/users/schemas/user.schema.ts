import * as mongoose from 'mongoose'

export const UserSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    avatar: String,
    tokenFCM: String
  },
  {
    versionKey: false,
    timestamps: true,
  },
)

/**
 * Hide user password in the responses
 */
UserSchema.methods.toJSON = function() {
  const user = this.toObject()
  delete user.password
  return user
}
