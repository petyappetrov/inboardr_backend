import * as mongoose from 'mongoose'

export const OfferSchema = new mongoose.Schema(
  {
    order: {
      type: mongoose.Types.ObjectId,
      ref: 'Order',
      required: true,
    },
    user: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    snowboard: {
      type: mongoose.Types.ObjectId,
      ref: 'Snowboard',
      required: true,
    },
    status: {
      type: String,
      enum: ['pending', 'accepted', 'closed', 'rejected', 'cancelled'],
      default: 'pending',
    },
    price: {
      type: Number,
      required: true,
    }
  },
  {
    timestamps: true,
  },
)
