import { Controller, Post, UseGuards, Body, Request, Get, Param, Put } from '@nestjs/common'
import { Request as RequestType } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { OffersService } from './offers.service'
import { CreateOfferInput } from './inputs/create-offer.input'
import { User } from '../users/interfaces/user.interface'

@Controller('offers')
export class OffersController {
  constructor(private readonly offersService: OffersService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  createBoots(
    @Body() input: CreateOfferInput,
    @Request() request: RequestType & { user: User },
  ) {
    return this.offersService.createOffer({ ...input, user: request.user._id })
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  getOffersByOrder(
    @Param('order') order: string
  ) {
    /**
     * TODO: Добавить валидацию
     * Получать офферы может только владелец заказа
     */
    return this.offersService.getOffersByOrder(order)
  }

  @Put('/accept/:id')
  @UseGuards(AuthGuard('jwt'))
  acceptOffer(
    @Param('id') id: string
  ) {
    /**
     * TODO: Добавить валидацию
     * Принимать оффер может только владелец заказа
     */
    return this.offersService.acceptOffer(id)
  }

  @Put('/cancel/:id')
  @UseGuards(AuthGuard('jwt'))
  cancelOffer(
    @Param('id') id: string,
  ) {
    /**
     * TODO: Добавить валидацию
     * Отменять оффер может только владелец оффера
     */
    return this.offersService.cancelOffer(id)
  }
}
