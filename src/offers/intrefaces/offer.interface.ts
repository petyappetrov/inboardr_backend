import { Document, Types } from 'mongoose'

export interface Offer extends Document {
  readonly order: Types.ObjectId
  readonly user: Types.ObjectId
  readonly snowboard: Types.ObjectId
  readonly price: number
  readonly status: 'pending' | 'accepted' | 'closed' | 'rejected' | 'cancelled'
}
