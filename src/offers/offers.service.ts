import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { Offer } from './intrefaces/offer.interface'
import { CreateOfferInput } from './inputs/create-offer.input'

@Injectable()
export class OffersService {
  constructor(
    @InjectModel('Offer') private readonly offerModel: Model<Offer>,
  ) {}

  createOffer(input: CreateOfferInput & { user: string }): Promise<Offer> {
    return this.offerModel.create(input);
  }

  getOffersByOrder(order: string): Promise<Offer[]> {
    return this.offerModel.find({ order }).exec()
  }

  acceptOffer(id: string): Promise<Offer> {
    return this.offerModel.findByIdAndUpdate(id, { status: 'accepted' }).exec()
  }

  closeOffer(id: string): Promise<Offer> {
    return this.offerModel.findByIdAndUpdate(id, { status: 'closed' }).exec()
  }

  cancelOffer(id: string): Promise<Offer> {
    return this.offerModel.findByIdAndUpdate(id, { status: 'canceled' }).exec()
  }

  rejectOffer(id: string): Promise<Offer> {
    return this.offerModel.findByIdAndUpdate(id, { status: 'rejected' }).exec()
  }
}
