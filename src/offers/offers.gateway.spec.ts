import { Test, TestingModule } from '@nestjs/testing';
import { OffersGateway } from './offers.gateway';

describe('OffersGateway', () => {
  let gateway: OffersGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OffersGateway],
    }).compile();

    gateway = module.get<OffersGateway>(OffersGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
