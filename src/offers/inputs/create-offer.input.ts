import {
  IsNotEmpty,
  IsString,
  IsNumber,
} from 'class-validator'

export class CreateOfferInput {
  @IsNotEmpty()
  @IsString()
  readonly order: string

  @IsNotEmpty()
  @IsString()
  readonly snowboard: string

  @IsNotEmpty()
  @IsNumber()
  readonly price: number
}
