import { SubscribeMessage, WebSocketGateway, OnGatewayConnection } from '@nestjs/websockets'
import { Socket } from 'dgram'
import { Logger } from '@nestjs/common'

@WebSocketGateway(3002)
export class OffersGateway {
  private logger: Logger = new Logger('AppGateway')

  // afterInit(server: Server) {
  //   this.logger.log('Init');
  // }

  handleDisconnect(client: Socket) {
    // this.logger.log(`Client disconnected: ${client.id}`);
  }

  handleConnection(client: Socket, ...args: any[]) {
    // this.logger.log(`Client connected: ${client.id}`);
  }

  @SubscribeMessage('message')
  handleMessage(client: Socket, payload: any): string {
    return 'Hello world!'
  }
}
