import { IsNotEmpty, MinLength } from 'class-validator'

export class LoginInput {
  @IsNotEmpty()
  @MinLength(6)
  readonly phone: string

  @IsNotEmpty()
  @MinLength(6)
  readonly password: string
}
