import { CreateUserInput } from '../../users/inputs/create-user.input'

export class RegisterInput extends CreateUserInput {}
