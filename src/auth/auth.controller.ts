import { Controller, Get, Post, Body, Request, UseGuards, Put } from '@nestjs/common'
import { Request as RequestType } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { AuthService } from './auth.service'
import { UsersService } from '../users/users.service'
import { LoginInput } from './inputs/login.input'
import { RegisterInput } from './inputs/register.input'
import { User } from '../users/interfaces/user.interface'
import { SnowboardsService } from '../snowboards/snowboards.service'

@Controller()
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly snowboardsService: SnowboardsService,
  ) {}

  @Post('login')
  login(
    @Body() input: LoginInput
  ) {
    return this.authService.login(input)
  }

  @Post('register')
  register(
    @Body() input: RegisterInput
  ) {
    return this.authService.register(input)
  }

  @Get('profile')
  @UseGuards(AuthGuard('jwt'))
  profile(
    @Request() request: RequestType & { user: User }
  ) {
    return this.usersService.getUserById(request.user._id)
  }

  @Put('profile/updateFCMToken')
  @UseGuards(AuthGuard('jwt'))
  updateFCMToken(
    @Request() request: RequestType & { user: User },
    @Body('token') token: string
  ) {
    return this.usersService.updateUserById(request.user._id, { tokenFCM: token })
  }

  @Get('profile/snowboards')
  @UseGuards(AuthGuard('jwt'))
  profileSnowboards(
    @Request() request: RequestType & { user: User }
  ) {
    return this.snowboardsService.getSnowboards({ owner: request.user._id })
  }
}
