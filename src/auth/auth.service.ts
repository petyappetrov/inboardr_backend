import { Injectable, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import * as bcrypt from 'bcrypt'
import { UsersService } from '../users/users.service'
import { LoginInput } from './inputs/login.input'
import { RegisterInput } from './inputs/register.input'

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async login(input: LoginInput) {
    const user = await this.usersService.getUserByPhone(input.phone)

    if (!user) {
      throw new UnauthorizedException('Пользователь с таким номером не найден')
    }

    const isMatch = await bcrypt.compare(input.password, user.password)

    if (!isMatch) {
      throw new UnauthorizedException('Неверный пароль')
    }

    return {
      token: this.jwtService.sign({ _id: user._id, phone: user.phone }),
    }
  }

  async register(input: RegisterInput) {
    const existUser = await this.usersService.getUserByPhone(input.phone)

    if (existUser) {
      throw new UnauthorizedException(
        'Пользователь с таким номером уже существует',
      )
    }

    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(input.password, salt)

    const user = await this.usersService.createUser({
      ...input,
      password: hash,
    })

    return {
      token: this.jwtService.sign({ _id: user._id, phone: user.phone }),
    }
  }
}
