import * as mongoose from 'mongoose'

export const NotificationSchema = new mongoose.Schema(
  {
    receiver: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    isReaded: {
      type: Boolean,
      default: false,
    },
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    },
    data: {
      type: JSON,
      required: false,
    },
    type: {
      type: String,
      enum: [
        'offer-created',
        'offer-canceled',
        'offer-closed',
        'offer-deleted',
        'order-created',
        'order-canceled',
        'order-closed',
        'order-deleted',
      ],
      required: true,
    },
  },
  {
    timestamps: true,
  },
)
