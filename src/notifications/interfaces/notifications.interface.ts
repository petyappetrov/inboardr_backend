import { Document, Types } from 'mongoose'

export enum NotificationTypes {
  OfferCreated = 'offer-created',
  OfferCanceled = 'offer-canceled',
  OfferClosed = 'offer-closed',
  OfferDeleted = 'offer-deleted',
  OrderCreated = 'order-created',
  OrderCanceled = 'order-canceled',
  OrderClosed = 'order-closed',
  OrderDeleted = 'order-deleted',
}

export interface Notification extends Document {
  readonly receiver: Types.ObjectId
  readonly title: string
  readonly type: NotificationTypes
  readonly body?: string
  readonly data?: any
}
