import { Controller, Get, UseGuards, Request, Put, Param } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { NotificationsService } from './notifications.service'
import { Request as RequestType } from 'express'
import { User } from 'src/users/interfaces/user.interface'

@Controller('notifications')
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  getNotificationsByReceiver(
    @Request() request: RequestType & { user: User },
  ) {
    return this.notificationsService.getNotificationsByReceiver(request.user._id)
  }

  @Put('read/:id')
  @UseGuards(AuthGuard('jwt'))
  markReadNotification(
    @Param('id') id: string
  ) {
    /**
     * TODO: Добавить валидацию
     * Отмечать "как прочитанный" может только владелец уведомелния
     */
    return this.notificationsService.markReadNotification(id)
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  unmarkReadNotification(
    @Param('id') id: string
  ) {
    /**
     * TODO: Добавить валидацию
     * Отмечать "как прочитанный" может только владелец уведомелния
     */
    return this.notificationsService.unmarkReadNotification(id)
  }

  @Get('/unreadCount')
  @UseGuards(AuthGuard('jwt'))
  getUnreadNotificationsCount(
    @Request() request: RequestType & { user: User },
  ) {
    return this.notificationsService.getUnreadNotificationsCount(request.user._id)
  }
}
