import {
  IsNotEmpty,
  IsOptional,
  IsEnum,
} from 'class-validator'
import { Types } from 'mongoose'
import { NotificationTypes } from '../interfaces/notifications.interface'

export class CreateNotificationInput {
  @IsNotEmpty()
  receiver: Types.ObjectId | string

  @IsNotEmpty()
  title: string

  @IsOptional()
  body?: string

  @IsOptional()
  data: any

  @IsNotEmpty()
  @IsEnum(NotificationTypes)
  type: NotificationTypes
}
