import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model, Types } from 'mongoose'
import { Notification } from './interfaces/notifications.interface'
import { FirebaseService } from '../firebase/firebase.service'
import { CreateNotificationInput } from './inputs/create-notification.input'
import { UsersService } from 'src/users/users.service'
import { FCMMessage } from 'src/firebase/interfaces/fcm-message.interface'

@Injectable()
export class NotificationsService {
  constructor(
    @InjectModel('Notification') private readonly notificationModel: Model<Notification>,
    private readonly firebaseService: FirebaseService,
    private readonly usersService: UsersService,
  ) {}

  getNotifications(): Promise<Notification[]> {
    return this.notificationModel.find({}).exec()
  }

  getNotificationsByReceiver(receiver: Types.ObjectId | string): Promise<Notification[]> {
    return this.notificationModel.find({ receiver }).exec()
  }

  getUnreadNotificationsCount(receiver: Types.ObjectId | string): Promise<Number> {
    return this.notificationModel.find({ receiver, isReaded: true  }).countDocuments().exec();
  }

  async createNotification(input: CreateNotificationInput): Promise<Notification> {
    const receiver = await this.usersService.getUserById(input.receiver)
    if (receiver.tokenFCM) {
      const FCMMessageFields: FCMMessage = { token: receiver.tokenFCM } as FCMMessage
      if (input.body) {
        FCMMessageFields.body = input.body
      }
      if (input.data) {
        FCMMessageFields.data = input.data
      }
      await this.firebaseService.send(FCMMessageFields)
    }
    return this.notificationModel.create(input)
  }

  markReadNotification(id: string): Promise<Notification> {
    return this.notificationModel.findByIdAndUpdate(id, { isReaded: true }).exec()
  }

  unmarkReadNotification(id: string): Promise<Notification> {
    return this.notificationModel.findByIdAndUpdate(id, { isReaded: false }).exec()
  }
}
