import * as mongoose from 'mongoose'

export const SnowboardSchema = new mongoose.Schema(
  {
    titleBoard: {
      type: String,
      required: true,
    },
    titleBoots: {
      type: String,
      required: true,
    },
    sizeBoard: {
      type: Number,
      required: true,
    },
    sizeBoots: {
      type: Number,
      required: true,
    },
    owner: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
    },
    photos: [
      {
        type: String,
        required: true,
      },
    ],
  },
  {
    versionKey: false,
    timestamps: true,
  },
)
