import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model, Types } from 'mongoose'
import { Snowboard } from './interfaces/snowboard.interface'
import { CreateSnowboardInput } from './inputs/create-snowboard.input'
import { UpdateSnowboardInput } from './inputs/update-snowboard.input'

@Injectable()
export class SnowboardsService {
  constructor(
    @InjectModel('Snowboard') private readonly snowboardModel: Model<Snowboard>,
  ) {}

  getSnowboardById(id: string | Types.ObjectId): Promise<Snowboard> {
    return this.snowboardModel.findById(id).exec()
  }

  getSnowboards(input = {}): Promise<Snowboard[]> {
    return this.snowboardModel.find(input).populate('owner').exec()
  }

  createSnowboard(input: CreateSnowboardInput & { owner: string }): Promise<Snowboard> {
    return this.snowboardModel.create(input)
  }

  updateSnowboard(id: string, input: UpdateSnowboardInput): Promise<Snowboard> {
    return this.snowboardModel.findByIdAndUpdate(id, input, { new: true }).exec()
  }

  deleteSnowboard(id: string): Promise<Snowboard> {
    return this.snowboardModel.findByIdAndDelete(id).exec()
  }
}
