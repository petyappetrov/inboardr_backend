import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { SnowboardsService } from './snowboards.service'
import { SnowboardSchema } from './schemas/snowboard.schema'
import { SnowboardsController } from './snowboards.controller'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Snowboard', schema: SnowboardSchema }]),
  ],
  providers: [SnowboardsService],
  controllers: [SnowboardsController],
  exports: [SnowboardsService]
})
export class SnowboardsModule {}
