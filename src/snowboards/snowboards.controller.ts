import {
  Controller,
  Post,
  Body,
  Param,
  Put,
  Delete,
  Get,
  UseGuards,
  Request,
  NotFoundException,
  NotAcceptableException,
} from '@nestjs/common'
import { Request as RequestType } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { SnowboardsService } from './snowboards.service'
import { CreateSnowboardInput } from './inputs/create-snowboard.input'
import { UpdateSnowboardInput } from './inputs/update-snowboard.input'
import { User } from '../users/interfaces/user.interface'

@Controller('snowboards')
export class SnowboardsController {
  constructor(private readonly snowboardsService: SnowboardsService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  createSnowboard(
    @Body() input: CreateSnowboardInput,
    @Request() request: RequestType & { user: User },
  ) {
    return this.snowboardsService.createSnowboard({ ...input, owner: request.user._id })
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async updateSnowboard(
    @Param('id') id: string,
    @Body() input: UpdateSnowboardInput,
    @Request() request: RequestType & { user: User },
  ) {
    const snowboard = await this.snowboardsService.getSnowboardById(id)
    if (!snowboard) {
      throw new NotFoundException()
    }
    if (!snowboard.owner.equals(request.user._id)) {
      throw new NotAcceptableException()
    }
    return this.snowboardsService.updateSnowboard(id, input)
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async deleteSnowboard(
    @Param('id') id: string,
    @Request() request: RequestType & { user: User },
  ) {
    const snowboard = await this.snowboardsService.getSnowboardById(id)
    if (!snowboard) {
      throw new NotFoundException()
    }
    if (!snowboard.owner.equals(request.user._id)) {
      throw new NotAcceptableException()
    }
    return this.snowboardsService.deleteSnowboard(id)
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  getSnowboardById(@Param('id') id: string) {
    return this.snowboardsService.getSnowboardById(id)
  }

  // TODO: Add user roles
  // Only administrators can get all snowboards
  @Get()
  @UseGuards(AuthGuard('jwt'))
  getSnowboards() {
    return this.snowboardsService.getSnowboards()
  }
}
