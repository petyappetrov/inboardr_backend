import { Test, TestingModule } from '@nestjs/testing';
import { SnowboardsController } from './snowboards.controller';

describe('Snowboard Controller', () => {
  let controller: SnowboardsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SnowboardsController],
    }).compile();

    controller = module.get<SnowboardsController>(SnowboardsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
