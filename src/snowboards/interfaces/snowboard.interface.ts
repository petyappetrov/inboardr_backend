import { Document, Types } from 'mongoose'
import { User } from '../../users/interfaces/user.interface';

export interface Snowboard extends Document {
  readonly titleBoard: string
  readonly titleBoots: string
  readonly sizeBoard: number
  readonly sizeBoots: number
  readonly owner: User
  readonly photos: string[]
}
