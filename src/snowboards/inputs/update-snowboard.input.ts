import { MinLength, IsArray } from 'class-validator'

export class UpdateSnowboardInput {
  @IsArray()
  photos?: string[]

  @MinLength(2)
  titleBoard?: string

  @MinLength(2)
  titleBoots?: string

  sizeBoard?: number

  sizeBoots?: number
}
