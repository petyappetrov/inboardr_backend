import {
  IsNotEmpty,
  MinLength,
  IsString,
  IsNumber,
  IsArray,
  IsUrl
} from 'class-validator'
export class CreateSnowboardInput {
  @IsNotEmpty()
  @MinLength(2)
  @IsString()
  titleBoard: string

  @IsNotEmpty()
  @MinLength(2)
  @IsString()
  titleBoots: string

  @IsNotEmpty()
  @IsNumber()
  sizeBoard: number

  @IsNotEmpty()
  @IsNumber()
  sizeBoots: number

  @IsNotEmpty()
  @IsArray()
  @IsUrl(
    {
      require_host: true,
      require_valid_protocol: true,
    },
    {
      each: true
    }
  )
  photos: string[]
}
